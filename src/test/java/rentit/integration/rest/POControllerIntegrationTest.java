package rentit.integration.rest;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import rentit.Application;
import rentit.SimpleDbConfig;
import rentit.integration.dto.PlantResource;
import rentit.integration.dto.PurchaseOrderResource;
import rentit.repositories.CustomerRepository;
import rentit.repositories.POUpdateRepository;
import rentit.repositories.PlantRepository;
import rentit.repositories.PurchaseOrderRepository;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class POControllerIntegrationTest {

	@Autowired
    private WebApplicationContext wac;
	
	@Autowired
    private PlantRepository plantRepo;
    
    @Autowired
    private PurchaseOrderRepository poRepo;
    
    @Autowired
    private CustomerRepository customerRepo;
    
    @Autowired
    private POUpdateRepository updateRepo;

    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
	
	@Test
    @DatabaseSetup(value="EmptyDatabase.xml")
    public void testCreatePurchaseOrder() throws Exception {
		PlantResource plant = new PlantResource();
        plant.setPlantId(10001L);
        //plant.add(new Link("http://rentit.com/rest/plants/10001"));
        plant.setPrice(300f);
		 
		 Calendar start = new GregorianCalendar(2014, 10, 6);
		 Calendar end = new GregorianCalendar(2014, 10, 10);
		 PurchaseOrderResource poResource = new PurchaseOrderResource();
		 poResource.setPlant(plant);
		 poResource.setStartDate(start.getTime());
		 poResource.setEndDate(end.getTime());
		 
		 MvcResult result = mockMvc.perform(post("/rest/pos")
		            .contentType(MediaType.APPLICATION_JSON)
		            .content(mapper.writeValueAsString(poResource))
		        )
		            .andExpect(status().isCreated())
		            .andReturn();
		 
		 PurchaseOrderResource poR = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
		 assertThat(poR.getLink("self"), is(notNullValue()));
		 assertThat(poR.get_link("acceptPO"), is(notNullValue()));
		 assertThat(poR.get_link("rejectPO"), is(notNullValue()));
		 assertThat(poR.getLinks().size()+poR.get_links().size(), is(3));
	}
	
	@Test
    @DatabaseSetup(value="DatabaseWithPendingPO.xml")
    public void testAcceptPurchaseOrder() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.get_link("acceptPO"), is(notNullValue()));
        
        Link accept = poResource.get_link("acceptPO");

        result = mockMvc.perform(post(accept.getHref()))
                .andExpect(status().isOk())
                .andReturn();
        
        poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.get_link("closePO"), is(notNullValue()));
        assertThat(poResource.get_link("requestPOUpdate"), is(notNullValue()));
        assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(3));
	}
	
	@Test
    @DatabaseSetup(value="DatabaseWithPendingPO.xml")
    public void testRejectPurchaseOrder() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.get_link("rejectPO"), is(notNullValue()));
        
        Link deleteURL = poResource.get_link("rejectPO");

        result = mockMvc.perform(delete(deleteURL
        		.getHref()))
                .andExpect(status().isOk())
                .andReturn();
        
        poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.get_link("updatePO"), is(notNullValue()));
        assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(2));
	}
	
	@Test
    @DatabaseSetup(value="DatabaseWithRejectedPO.xml")
    public void testUpdatePurchaseOrder() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.get_link("updatePO"), is(notNullValue()));
        
        Link updateLink = poResource.get_link("updatePO");
        
        result = mockMvc.perform(put(updateLink.getHref())
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(mapper.writeValueAsString(poResource))
        		)
                .andExpect(status().isOk())
                .andReturn();
        
        poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.get_link("acceptPO"), is(notNullValue()));
		assertThat(poResource.get_link("rejectPO"), is(notNullValue()));
		assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(3));
	}
	
	@Test
    @DatabaseSetup(value="DatabaseWithOpenPO.xml")
    public void testClosePurchaseOrder() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.get_link("closePO"), is(notNullValue()));
        
        Link close = poResource.get_link("closePO");
        
        result = mockMvc.perform(delete(close.getHref()))
                .andExpect(status().isOk())
                .andReturn();
        
        poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(1));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithOpenPO.xml")
	public void testRequestPurchaseOrderUpdate() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.get_link("requestPOUpdate"), is(notNullValue()));
        
        Link requestLink = poResource.get_link("requestPOUpdate");
        
        result = mockMvc.perform(post(requestLink.getHref()))
                .andExpect(status().isOk())
                .andReturn();
        
        poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.get_link("acceptPOUpdate"), is(notNullValue()));
        assertThat(poResource.get_link("rejectPOUpdate"), is(notNullValue()));
        assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(3));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithPendingUpdate.xml")
	public void testAcceptPurchaseOrderUpdate() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
		assertThat(poResource.get_link("acceptPOUpdate"), is(notNullValue()));
		
		Link accept = poResource.get_link("acceptPOUpdate");
		
		result = mockMvc.perform(post(accept.getHref()))
                .andExpect(status().isOk())
                .andReturn();
		
		poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.get_link("closePO"), is(notNullValue()));
        assertThat(poResource.get_link("requestPOUpdate"), is(notNullValue()));
        assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(3));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithPendingUpdate.xml")
	public void testRejectPurchaseOrderUpdate() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/pos/{id}", 10001L))
                .andExpect(status().isOk())
                .andReturn();
		
		PurchaseOrderResource poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
		assertThat(poResource.get_link("rejectPOUpdate"), is(notNullValue()));
		
		Link accept = poResource.get_link("rejectPOUpdate");
		
		result = mockMvc.perform(post(accept.getHref()))
                .andExpect(status().isOk())
                .andReturn();
		
		poResource = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderResource.class);
        assertThat(poResource.getLink("self"), is(notNullValue()));
        assertThat(poResource.get_link("closePO"), is(notNullValue()));
        assertThat(poResource.get_link("requestPOUpdate"), is(notNullValue()));
        assertThat(poResource.getLinks().size()+poResource.get_links().size(), is(3));
	}
}
