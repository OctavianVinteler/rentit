package rentit.integration.rest;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import rentit.Application;
import rentit.SimpleDbConfig;
import rentit.integration.dto.PlantResource;
import rentit.integration.dto.PurchaseOrderResource;
import rentit.models.Plant;
import rentit.models.PurchaseOrderStatus;
import rentit.repositories.PlantRepository;
import rentit.repositories.PurchaseOrderRepository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class PlantRestControllerTests {
    
	@Autowired
    private WebApplicationContext wac;

    @Autowired
    private PlantRepository plantRepo;
    
    @Autowired
    private PurchaseOrderRepository poRepo;
    
    private MockMvc mockMvc;
    
    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetAllPlants() throws Exception {
		mockMvc.perform(get("/rest/plants"))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$", hasSize(14)));
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetAnExistingPlant() throws Exception {
		Plant plant = plantRepo.findOne(10001L);
		
		mockMvc.perform(get("/rest/plants/{id}", 10001L))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.name", is(plant.getName())))
        	.andExpect(jsonPath("$.description", is(plant.getDescription())))
        	.andExpect(jsonPath("$.price", is(plant.getPrice().doubleValue())));
	}

	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetANonExistingPlant() throws Exception {
		mockMvc.perform(get("/rest/plants/{id}", 10015L))
        	.andExpect(status().isNotFound());
	}
		
	@Test
	@DatabaseSetup("dataset.xml")
	public void testCreatePO() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		Calendar start = new GregorianCalendar(2014, 8, 22);
		Calendar end = new GregorianCalendar(2014, 8, 24);
		PlantResource plantResource = new PlantResource();
		plantResource.setPlantId(10006L);
		plantResource.setName("Excavator");
		plantResource.setDescription("20 Tonne Large excavator");
		plantResource.setPrice(450.0F);

		PurchaseOrderResource purchaseOrderResource = new PurchaseOrderResource();
		purchaseOrderResource.setStartDate(start.getTime());
		purchaseOrderResource.setEndDate(end.getTime());
		purchaseOrderResource.setPlant(plantResource);
		//purchaseOrderResource.setId(5000L);
		purchaseOrderResource.setPoId(5000L);
		
		long count = poRepo.count();
		
		mockMvc.perform(post("/rest/pos")
				//.param("poJSON", mapper.writeValueAsString(purchaseOrderResource))
				.content(mapper.writeValueAsString(purchaseOrderResource))
				.contentType(MediaType.APPLICATION_JSON)
			)
	        	.andExpect(status().isCreated());
		assertThat("Table size", poRepo.count(), equalTo(count + 1));
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testCreatePOWithNotAvailablePlant() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		Calendar start = new GregorianCalendar(2014, 9, 22);
		Calendar end = new GregorianCalendar(2014, 9, 24);
        PlantResource plantResource = new PlantResource();
        plantResource.setPlantId(10006L);
		plantResource.setName("Excavator");
		plantResource.setDescription("20 Tonne Large excavator");
		plantResource.setPrice(450.0F);
		
		PurchaseOrderResource purchaseOrderResource = new PurchaseOrderResource();
		purchaseOrderResource.setStartDate(start.getTime());
		purchaseOrderResource.setEndDate(end.getTime());
		purchaseOrderResource.setPlant(plantResource);
		purchaseOrderResource.setPoId(6000L);
		
		long count = poRepo.count();
		
		mockMvc.perform(post("/rest/pos")
				//.param("poJSON", mapper.writeValueAsString(purchaseOrderResource))
				.content(mapper.writeValueAsString(purchaseOrderResource))
				.contentType(MediaType.APPLICATION_JSON)
			)
				.andExpect(status().reason("Plant not available!"))
	        	.andExpect(status().isBadRequest());
		
		assertThat("Table size", poRepo.count(), equalTo(count));
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetAllPurchaseOrders() throws Exception {
		mockMvc.perform(get("/rest/pos"))
    	.andExpect(status().isOk())
    	.andExpect(jsonPath("$", hasSize(1)));
	}
		
}