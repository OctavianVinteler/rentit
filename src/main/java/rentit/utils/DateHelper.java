package rentit.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

	public static Date getDateFromString(String dateFormat, String date) {
		DateFormat df = new SimpleDateFormat(dateFormat);
		Date dateCast = null;
		try{
			dateCast = df.parse(date);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return dateCast;
	}
	
	public static int getDaysDifference(Date startDate, Date endDate)
	{
		if(startDate.getTime() >= endDate.getTime())
		{
			return 0;
		}
		
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		startDate = c.getTime();
		
		c.setTime(endDate);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		endDate = c.getTime();
		
		return ((int) (endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	public static Date addDays(Date date, int numberOfDays)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, numberOfDays);
		
		return c.getTime();
	}
	
	public static String getStringFromDate(Date date, String dateFormat)
	{
		DateFormat df = new SimpleDateFormat(dateFormat);
		
		return df.format(date);
	}
}
