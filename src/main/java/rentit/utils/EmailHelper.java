package rentit.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import rentit.integration.dto.InvoiceResource;
import rentit.models.Invoice;

public class EmailHelper {
	
	public void sendEmail(InvoiceResource invoice, JavaMailSenderImpl sender, Boolean reminder, String email, Boolean xml) throws Exception
	{
		MimeMessage message = sender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		//helper.setTo("octa.esi@gmail.com");
		helper.setTo(email);

		if(reminder)
		{
			helper.setText("We would like to remind you that the invoice attached to this e-mail has not been paid");
		}
		else
		{
			helper.setText("Invoice");
		}
		
		JAXBContext context = JAXBContext.newInstance(InvoiceResource.class);
        Marshaller m = context.createMarshaller();
        //for pretty-print XML in JAXB
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to File
        File file = new File("invoice.xml");
        m.marshal(invoice, file);
        
        if(xml)
        {
        	InputStream str = FileUtils.openInputStream(file);
        	helper.addAttachment("invoice-" + invoice.getInvoiceId() + ".xml", new ByteArrayResource(IOUtils.toByteArray(str)), "application/xml");
        }
        else
        {
        	helper.addAttachment("invoice-" + invoice.getInvoiceId() + ".xml", file);
        }

		sender.send(message);
	}
}
