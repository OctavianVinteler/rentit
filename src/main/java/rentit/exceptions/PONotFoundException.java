package rentit.exceptions;

public class PONotFoundException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public PONotFoundException(Long id){
		super(String.format("Purchase Order not available! (Purchase Order id: %d)", id));
	}
}
