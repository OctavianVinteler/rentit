package rentit.exceptions;

public class PlantNotAvailableException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlantNotAvailableException(Long id){
		super(String.format("Plant not available! (Plant id: %d)", id));
	}
}
