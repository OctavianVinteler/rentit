package rentit.exceptions;

import java.util.Date;

public class NegativeTimeIntervalException extends Exception {
	private static final long serialVersionUID = 1L;

	public NegativeTimeIntervalException(Date startDate, Date endDate)
	{
		super(String.format("Negative time Interval! (Start Date: %s -- End Date: %s)", startDate.toString(), endDate.toString()));
	}
}
