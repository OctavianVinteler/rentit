package rentit.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rentit.security.NewUserData;

@Controller
public class SecurityController {  
	
	@Autowired
	private DataSource dataSource;
	
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/welcome")
    public String home() {
        return "welcome";
    }
    
    @RequestMapping("/register")
	public String register(Model model) {
		model.addAttribute("newUser", new NewUserData());
		return "register";
	}
	
	@RequestMapping(value="/registerUser", method=RequestMethod.POST)
	public String registerUser(NewUserData user){
		
		if(!user.getPassword().equals(user.getSecondPassword()))
		{
			return "redirect:/register";
		}
		
		try {
			String sql = "insert into users (username, password, enabled) VALUES (?, ?, true)";
			String sql2 = "insert into authorities (username, authority) VALUES (?, ?)";
			
			Connection conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.executeUpdate();
			ps.close();
			
			ps = conn.prepareStatement(sql2);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getRole());
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			return "redirect:/register";
		}
		
		return "redirect:/login";
	}
}
