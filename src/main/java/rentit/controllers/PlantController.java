package rentit.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import rentit.integration.dto.PlantResourceAssembler;
import rentit.repositories.PlantRepository;

@Controller
@RequestMapping("/plants")
public class PlantController {

	@Autowired
	PlantRepository plantRepo;
	
	PlantResourceAssembler plantAssembler = new PlantResourceAssembler();
	
	@RequestMapping("")
	public String getPlants(Model model)
	{
		model.addAttribute("plants", plantRepo.findAll());
		return "plants/list";
	}
	
	@RequestMapping("/{id}")
	public String show(Model model, @PathVariable Long id){
		model.addAttribute("plant", plantRepo.getOne(id));
		return "plants/show";
	}
	
	@RequestMapping("/{id}/available")
	public String availability(Model model, @PathVariable Long id){
		model.addAttribute("plant", plantRepo.getOne(id));
		return "plants/available";
	}
}
