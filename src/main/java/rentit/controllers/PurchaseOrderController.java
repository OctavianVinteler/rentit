package rentit.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import rentit.integration.dto.InvoiceResource;
import rentit.integration.dto.InvoiceResourceAssembler;
import rentit.integration.dto.PurchaseOrderResource;
import rentit.integration.dto.PurchaseOrderResourceAssembler;
import rentit.models.Invoice;
import rentit.models.POUpdateRequest;
import rentit.models.PurchaseOrder;
import rentit.models.PurchaseOrderStatus;
import rentit.models.URStatus;
import rentit.repositories.InvoiceRepository;
import rentit.repositories.POUpdateRepository;
import rentit.repositories.PurchaseOrderRepository;
import rentit.utils.DateHelper;
import rentit.utils.EmailHelper;

@Controller
@RequestMapping("/pos")
public class PurchaseOrderController {

	@Autowired
	PurchaseOrderRepository poRepo;
	
	@Autowired
	InvoiceRepository invRepo;
	
	@Autowired
	POUpdateRepository updateRepo;
	
	InvoiceResourceAssembler invAssembler = new InvoiceResourceAssembler();
	
	PurchaseOrderResourceAssembler poAssembler = new PurchaseOrderResourceAssembler();
	
	@Autowired
	private JavaMailSenderImpl sender;
	
	@RequestMapping("")
	public String getPOS(Model model){
		model.addAttribute("pos", poAssembler.toResource(poRepo.findAll()));
		return "pos/list";
	}
	
	@RequestMapping("{id}/invoice")
	public String sendInvoice(Model model, @PathVariable Long id){
		
		PurchaseOrder po = poRepo.getOne(id);
		
		Invoice invoice = new Invoice();
		invoice.setPayed(false);
		invoice.setStartDate(po.getStartDate());
		
		Date endDate = po.getEndDate();
		
		if(po.getUpdates() != null)
		{
			for(POUpdateRequest update : po.getUpdates())
			{
				if(update.getEndDate().after(endDate) && update.getStatus() == URStatus.ACCEPTED)
				{
					endDate = update.getEndDate(); 
				}
			}
		}
		
		invoice.setEndDate(endDate);
		invoice.setDueDate(DateHelper.addDays(new Date(), 10));
		invoice.setLatePayment(false);
		invoice.setTotalCost(po.getPlant().getPrice() * (DateHelper.getDaysDifference(invoice.getStartDate(), invoice.getEndDate()) + 1));
		
		po.setInvoice(invoice);
		
		invRepo.saveAndFlush(invoice);
		poRepo.saveAndFlush(po);
		
		PurchaseOrderResource poRes = poAssembler.toResource(po);
		invoice.setPurchaseOrderRef(poRes.getLink("self").getHref());
		
		invRepo.saveAndFlush(invoice);

		InvoiceResource invRes = invAssembler.toResource(invoice);
		
		EmailHelper email = new EmailHelper();
		try {
			//if(invoice.getPurchaseOrderRef().contains("rentit-1102"))
			if(po.getCustomer().getName().contains("rentit-1102"))
			{
				email.sendEmail(invRes, sender, false, "octa.esi@gmail.com", false);
			}
			//else if(invoice.getPurchaseOrderRef().contains("rentit-team3"))
			else if(po.getCustomer().getName().contains("buildit-team3"))
			{
				email.sendEmail(invRes, sender, false, "buildit.team3@gmail.com", true);
			}
			else if(po.getCustomer().getName().contains("team2"))
			{
				email.sendEmail(invRes, sender, false, "esigroup2.buildit@gmail.com", true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/pos";
	}
	
	@RequestMapping("{id}/reminder")
	public String sendReminder(Model model, @PathVariable Long id){
		
		PurchaseOrder po = poRepo.getOne(id);
		
		PurchaseOrderResource poRes = poAssembler.toResource(po);
		
		Invoice inv = invRepo.finderMethod(poRes.getLink("self").getHref());
		
		InvoiceResource invRes = invAssembler.toResource(inv);
		
		EmailHelper email = new EmailHelper();
		try {
			//if(invRes.getPurchaseOrderRef().contains("rentit-1102"))
			if(po.getCustomer().getName().contains("rentit-1102"))
			{
				email.sendEmail(invRes, sender, true, "octa.esi@gmail.com", false);
			}
			else if(po.getCustomer().getName().contains("buildit-team3"))
			//else if(invRes.getPurchaseOrderRef().contains("rentit-team3"))
			{
				email.sendEmail(invRes, sender, true, "buildit.team3@gmail.com", true);
			}
			else if(po.getCustomer().getName().contains("team2"))
			{
				email.sendEmail(invRes, sender, true, "esigroup2.buildit@gmail.com", true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/pos";
	}
	
	@RequestMapping("{id}/approve")
	public String approvePO(Model model, @PathVariable Long id)
	{
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.OPEN);
		
		poRepo.saveAndFlush(po);
		
		return "redirect:/pos";
	}
	
	@RequestMapping("{id}/reject")
	public String rejectPO(Model model, @PathVariable Long id)
	{
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.REJECTED);
		
		poRepo.saveAndFlush(po);
		
		return "redirect:/pos";
	}
	
	@RequestMapping("{id}/close")
	public String closePO(Model model, @PathVariable Long id)
	{
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.CLOSED);
		
		poRepo.saveAndFlush(po);
		
		return "redirect:/pos";
	}
	
	@RequestMapping("{poid}/updates/{upid}/accept")
	public String acceptPoUpdate(Model model, @PathVariable Long poid, @PathVariable Long upid)
	{
		PurchaseOrder po = poRepo.findOne(poid);
		
		POUpdateRequest updateRequest = null;
		
		for(POUpdateRequest up : po.getUpdates())
		{
			if(up.getId().longValue() == upid.longValue())
			{
				updateRequest = up;
				break;
			}
		}
		
		if(updateRequest != null)
		{
			updateRequest.setStatus(URStatus.ACCEPTED);
			updateRepo.saveAndFlush(updateRequest);
		}
		po.setStatus(PurchaseOrderStatus.OPEN);
		
		poRepo.saveAndFlush(po);
		
		return "redirect:/pos";
	}
	
	@RequestMapping("{poid}/updates/{upid}/reject")
	public String rejectPoUpdate(Model model, @PathVariable Long poid, @PathVariable Long upid)
	{
		PurchaseOrder po = poRepo.findOne(poid);
		
		POUpdateRequest updateRequest = null;
		
		for(POUpdateRequest up : po.getUpdates())
		{
			if(up.getId().longValue() == upid.longValue())
			{
				updateRequest = up;
				break;
			}
		}
		
		if(updateRequest != null)
		{
			updateRequest.setStatus(URStatus.REJECTED);
			updateRepo.saveAndFlush(updateRequest);
		}
		po.setStatus(PurchaseOrderStatus.OPEN);
		
		poRepo.saveAndFlush(po);
		
		return "redirect:/pos";
	}
}
