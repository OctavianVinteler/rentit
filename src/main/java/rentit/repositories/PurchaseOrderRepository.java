package rentit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rentit.models.Invoice;
import rentit.models.PurchaseOrder;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long>{

	@Query("select p from PurchaseOrder p " +
	           "where LOWER(p.invoice.purchaseOrderRef) like LOWER(:poRef)")
	    PurchaseOrder finderMethod(@Param("poRef") String poRef);
}
