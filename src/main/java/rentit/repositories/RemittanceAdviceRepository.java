package rentit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import rentit.models.RemittanceAdvice;

public interface RemittanceAdviceRepository extends JpaRepository<RemittanceAdvice, Long> {

	@Query("select r from RemittanceAdvice r " +
	           "where LOWER(r.purchaseOrderRef) like LOWER(:poRef)")
	    RemittanceAdvice finderMethod(@Param("poRef") String poRef);
}
