package rentit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rentit.models.POUpdateRequest;

@Repository
public interface POUpdateRepository extends JpaRepository<POUpdateRequest, Long>{

}
