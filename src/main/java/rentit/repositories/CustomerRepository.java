package rentit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rentit.models.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
