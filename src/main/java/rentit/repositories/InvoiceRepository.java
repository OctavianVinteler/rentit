package rentit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import rentit.models.Invoice;
import rentit.models.RemittanceAdvice;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
	
	@Query("select i from Invoice i " +
	           "where LOWER(i.purchaseOrderRef) like LOWER(:poRef)")
	    Invoice finderMethod(@Param("poRef") String poRef);

}
