package rentit.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rentit.models.Plant;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

    List<Plant> findByNameLike(String name);

    @Query("select p from Plant p " +
           "where LOWER(p.name) like LOWER(:name) and p.price between :minimum and :maximum")
    List<Plant> finderMethod(@Param("name") String name, 
                             @Param("minimum") Float minimum,
                             @Param("maximum") Float maximum);
    
    /*@Query("select p from Plant p where (LOWER(p.name) like LOWER(:name)) and p.id NOT IN"
			+ "(select distinct p.id from Plant p, PurchaseOrder o " + 
			"where p.id = o.plant.id and NOT ((o.startDate <= :minDate and :minDate <= o.endDate)  or (o.startDate <= :maxDate and :maxDate <= o.endDate)"
			+ " or (:minDate <= o.startDate and :maxDate >= o.endDate)))")
	List<Plant> findAvailablePlants(@Param("minDate") @Temporal Date minimum, @Param("maxDate") @Temporal Date maximum, @Param("name") String name);*/
    
    
    @Query("select p from Plant p where p.name = :name and p NOT IN"
	+ "(select o.plant from PurchaseOrder o " + 
	"where o.startDate BETWEEN :minDate and :maxDate or o.endDate BETWEEN :minDate and :maxDate)")
    List<Plant> findAvailablePlants(@Param("minDate") @Temporal Date minimum, @Param("maxDate") @Temporal Date maximum, @Param("name") String name);
}