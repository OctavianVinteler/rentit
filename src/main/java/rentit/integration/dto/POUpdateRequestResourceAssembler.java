package rentit.integration.dto;

import java.util.ArrayList;
import java.util.List;

import rentit.models.POUpdateRequest;

public class POUpdateRequestResourceAssembler {
	
	public POUpdateRequestResource toResource(POUpdateRequest request){
		POUpdateRequestResource res = new POUpdateRequestResource();
		
		res.setUpdateId(request.getId());
		res.setEndDate(request.getEndDate());
		res.setStatus(request.getStatus());
		return res;
	}
	
	public List<POUpdateRequestResource> toResource(List<POUpdateRequest> requests){
		if(requests == null)
		{
			return null;
		}
		
		List<POUpdateRequestResource> ress = new ArrayList<POUpdateRequestResource>();
		
		for(POUpdateRequest request : requests){
			ress.add(toResource(request));
		}
		
		return ress;
	}
}
