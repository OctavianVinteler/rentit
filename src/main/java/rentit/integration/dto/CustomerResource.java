package rentit.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import lombok.Data;

@Data
@XmlRootElement(name="customer")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResource {
	Long id;
    String name;
    String vatNumber;
}
