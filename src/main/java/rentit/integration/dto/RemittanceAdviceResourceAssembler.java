package rentit.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentit.integration.rest.PurchaseOrderRestController;
import rentit.models.RemittanceAdvice;

public class RemittanceAdviceResourceAssembler extends ResourceAssemblerSupport<RemittanceAdvice, RemittanceAdviceResource> {

	public RemittanceAdviceResourceAssembler() {
	    super(PurchaseOrderRestController.class, RemittanceAdviceResource.class);
	}
	
	public RemittanceAdviceResource toResource(RemittanceAdvice ra){		
		if(ra == null)
		{
			return null;
		}
		
		RemittanceAdviceResource res = createResourceWithId(ra.getId(), ra);
		res.setRemittanceAdviceId(ra.getId());
		res.setDatePayed(ra.getDatePayed());
		res.setAmountPayed(ra.getAmountPayed());
		res.setPurchaseOrderRef(ra.getPurchaseOrderRef());
		
		return res;
	}
	
	public List<RemittanceAdviceResource> toResource(List<RemittanceAdvice> ras){
		List<RemittanceAdviceResource> ress = new ArrayList<RemittanceAdviceResource>();
		
		for(RemittanceAdvice ra : ras)
		{
			ress.add(toResource(ra));
		}
		
		return ress;
	}
}
