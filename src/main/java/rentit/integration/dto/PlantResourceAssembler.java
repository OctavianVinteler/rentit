package rentit.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentit.integration.rest.PlantRestController;
import rentit.models.Plant;

public class PlantResourceAssembler extends ResourceAssemblerSupport<Plant, PlantResource> {
	
	public PlantResourceAssembler() {
	    super(PlantRestController.class, PlantResource.class);
	}
	
	public PlantResource toResource(Plant plant) {
		PlantResource res = createResourceWithId(plant.getPlantId(), plant);
		res.setPlantId(plant.getPlantId());
		res.setName(plant.getName());
		res.setDescription(plant.getDescription());
		res.setPrice(plant.getPrice());
		return res;
	}
	public List<PlantResource> toResource(List<Plant> plants) {
		List<PlantResource> ress = new ArrayList<>();
		
		for (Plant plant: plants)
			ress.add(toResource(plant));
		return ress;
	}
}
