	package rentit.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentit.integration.rest.PurchaseOrderRestController;
import rentit.models.POUpdateRequest;
import rentit.models.PurchaseOrder;
import rentit.models.URStatus;
import rentit.utils.DateHelper;
import rentit.utils.ExtendedLink;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;;

public class PurchaseOrderResourceAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderResource> {

	private PlantResourceAssembler plantAssembler = new PlantResourceAssembler();
	private POUpdateRequestResourceAssembler updateAssembler = new POUpdateRequestResourceAssembler();
	private InvoiceResourceAssembler invoiceAssembler = new InvoiceResourceAssembler();
	private RemittanceAdviceResourceAssembler remittanceAdviceAssembler = new RemittanceAdviceResourceAssembler();
	
	public PurchaseOrderResourceAssembler() {
	    super(PurchaseOrderRestController.class, PurchaseOrderResource.class);
	}
	
	/*public PurchaseOrderResource toResource(PurchaseOrder po){
		PurchaseOrderResource res = new PurchaseOrderResource();
		res.setPoId(po.getId());
		res.setStartDate(po.getStartDate());
		res.setEndDate(po.getEndDate());
		res.setPlant(plantAssembler.toResource(po.getPlant()));
		return res;
	}*/
	
	public PurchaseOrderResource toResource(PurchaseOrder po){
		PurchaseOrderResource res = createResourceWithId(po.getId(), po);
		res.setPoId(po.getId());
		res.setStartDate(po.getStartDate());
		res.setEndDate(po.getEndDate());
		res.setPlant(plantAssembler.toResource(po.getPlant()));
		res.setUpdates(updateAssembler.toResource(po.getUpdates()));
		res.setInvoice(invoiceAssembler.toResource(po.getInvoice()));
		res.setRemittanceAdvice(remittanceAdviceAssembler.toResource(po.getRemittanceAdvice()));
		res.setCost(po.getCost());
		res.setStatus(po.getStatus());
		
		if(po.getUpdates() != null)
		{
			for(POUpdateRequest up : po.getUpdates())
			{
				if(up.getStatus() == URStatus.ACCEPTED && up.getEndDate().after(res.getEndDate()))
				{
					res.setEndDate(up.getEndDate());
					res.setCost(po.getPlant().getPrice() * (DateHelper.getDaysDifference(res.getStartDate(), res.getEndDate()) + 1));
				}
			}
		}
		
		switch (po.getStatus()) {
	    case PENDING_CONFIRMATION:
	    	res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).acceptPO(po.getId())).toString(), "rejectPO", "DELETE"));
	        res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).acceptPO(po.getId())).toString(), "acceptPO", "POST"));
	        break;
	    case REJECTED:
	    	try {
				res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).updatePO(res, po.getId())).toString(), "updatePO", "PUT"));
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	break;
	    case OPEN:
	    	res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).closePO(po.getId())).toString(), "closePO", "DELETE"));
	    	res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).requestPOUpdate(po.getId(), DateHelper.addDays(po.getEndDate(), 1))).toString(), "requestPOUpdate", "POST"));
	    	break;
	    case CLOSED:
	    	break;
	    case PENDING_UPDATE:
	    	POUpdateRequest updateRequest = null;
	    	
	    	for(POUpdateRequest req : po.getUpdates())
	    	{
	    		if(req.getStatus() == URStatus.OPEN)
	    		{
	    			updateRequest = req;
	    		}
	    	}
	    	
	    	if(updateRequest != null)
	    	{
	    		res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).acceptPOUpdate(po.getId(), updateRequest.getId())).toString(), "acceptPOUpdate", "POST"));
	    		res.add(new ExtendedLink(linkTo(methodOn(PurchaseOrderRestController.class).rejectPOUpdate(po.getId(), updateRequest.getId())).toString(), "rejectPOUpdate", "DELETE"));
	    	}
	    	break;
	    default:
	        break;
	    }
		
		return res;
	}
	
	public List<PurchaseOrderResource> toResource(List<PurchaseOrder> pos){
		List<PurchaseOrderResource> ress = new ArrayList<PurchaseOrderResource>();
		
		for(PurchaseOrder po: pos)
		{
			ress.add(toResource(po));
		}
		
		return ress;
	}
}
