package rentit.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import rentit.utils.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@XmlRootElement(name="remittanceAdvice")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemittanceAdviceResource extends ResourceSupport {

	Long remittanceAdviceId;
	Date datePayed;
	Float amountPayed;
	String purchaseOrderRef;
}
