package rentit.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import rentit.utils.ResourceSupport;

@Setter
@Getter
@XmlRootElement(name="invoice")
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoiceResource  extends ResourceSupport {

	Long invoiceId;
	
	Boolean payed;
	Date startDate;
	Date endDate;
	Date dueDate;
	Float totalCost;
	String purchaseOrderRef;
	Boolean latePayment;
}
