package rentit.integration.dto;

import java.util.ArrayList;
import java.util.List;

import rentit.models.Customer;

public class CustomerResourceAssembler {
	public CustomerResource toResource(Customer customer) {
		CustomerResource res = new CustomerResource();
		res.setId(customer.getId());
		res.setName(customer.getName());
		res.setVatNumber(customer.getVatNumber());
		return res;
	}
	public List<CustomerResource> toResource(List<Customer> customers) {
		List<CustomerResource> ress = new ArrayList<>();
		
		for (Customer customer : customers)
			ress.add(toResource(customer));
		return ress;
	}
}
