package rentit.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import rentit.integration.rest.PurchaseOrderRestController;
import rentit.models.Invoice;

public class InvoiceResourceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceResource> {

	public InvoiceResourceAssembler() {
	    super(PurchaseOrderRestController.class, InvoiceResource.class);
	}
	
	public InvoiceResource toResource(Invoice inv){
		
		if(inv == null)
		{
			return null;
		}
		
		InvoiceResource res = createResourceWithId(inv.getId(), inv);
		res.setInvoiceId(inv.getId());
		res.setPayed(inv.getPayed());
		res.setStartDate(inv.getStartDate());
		res.setEndDate(inv.getEndDate());
		res.setDueDate(inv.getDueDate());
		res.setTotalCost(inv.getTotalCost());
		res.setPurchaseOrderRef(inv.getPurchaseOrderRef());
		res.setLatePayment(inv.getLatePayment());
		
		return res;
	}
	
	public List<InvoiceResource> toResource(List<Invoice> invs){
		List<InvoiceResource> ress = new ArrayList<InvoiceResource>();
		
		for(Invoice inv: invs)
		{
			ress.add(toResource(inv));
		}
		
		return ress;
	}
}
