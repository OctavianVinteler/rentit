package rentit.integration.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import rentit.models.PurchaseOrderStatus;
import rentit.utils.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@XmlRootElement(name="purchaseOrder")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseOrderResource extends ResourceSupport{

	Long poId;
	Date startDate;
	Date endDate;
	PlantResource plant;
	CustomerResource customer;
	List<POUpdateRequestResource> updates;
	Float cost;
	InvoiceResource invoice;
	RemittanceAdviceResource remittanceAdvice;
	PurchaseOrderStatus status;
}
