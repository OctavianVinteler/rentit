package rentit.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import rentit.models.URStatus;
import rentit.utils.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name="poUpdateRequest")
@JsonIgnoreProperties(ignoreUnknown = true)
public class POUpdateRequestResource extends ResourceSupport {
	
	Long updateId;
	Date endDate;
	URStatus status;
}
