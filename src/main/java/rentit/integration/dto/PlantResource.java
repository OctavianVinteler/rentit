package rentit.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import rentit.utils.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@XmlRootElement(name="plant")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlantResource  extends ResourceSupport {
	Long plantId;
    String name;
    String description;
    Float price;
}
