package rentit.integration.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rentit.exceptions.NegativeTimeIntervalException;
import rentit.exceptions.PONotFoundException;
import rentit.exceptions.PlantNotAvailableException;
import rentit.integration.dto.PurchaseOrderResource;
import rentit.integration.dto.PurchaseOrderResourceAssembler;
import rentit.models.Customer;
import rentit.models.POUpdateRequest;
import rentit.models.Plant;
import rentit.models.PurchaseOrder;
import rentit.models.PurchaseOrderStatus;
import rentit.models.URStatus;
import rentit.repositories.CustomerRepository;
import rentit.repositories.POUpdateRepository;
import rentit.repositories.PlantRepository;
import rentit.repositories.PurchaseOrderRepository;
import rentit.utils.DateHelper;

@RestController
@RequestMapping("/rest/pos")
public class PurchaseOrderRestController {

	@Autowired
	PurchaseOrderRepository poRepo;
	
	@Autowired
	PlantRepository plantRepo;
	
	@Autowired
	POUpdateRepository updateRepo;
	
	@Autowired
	CustomerRepository customerRepo;
	
	PurchaseOrderResourceAssembler purchaseOrderResourceAssambler = new PurchaseOrderResourceAssembler();
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public PurchaseOrderResource createPO(@RequestBody PurchaseOrderResource poResource) throws Exception {
		
		if(poResource.getStartDate().after(poResource.getEndDate())) {
			throw new NegativeTimeIntervalException(poResource.getStartDate(), poResource.getEndDate());
		}
		
		PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(poResource.getStartDate());
		po.setEndDate(poResource.getEndDate());
		po.setPlant(plantRepo.getOne(poResource.getPlant().getPlantId()));
		po.setCost(po.getPlant().getPrice() * (DateHelper.getDaysDifference(po.getStartDate(), po.getEndDate()) + 1));
		po.setStatus(PurchaseOrderStatus.PENDING_CONFIRMATION);
		
		Customer customer = new Customer();
		customer.setId(poResource.getCustomer().getId());
		customer.setName(poResource.getCustomer().getName());
		customer.setVatNumber(poResource.getCustomer().getVatNumber());
		
		po.setCustomer(customer);
		
		List<Plant> plants = plantRepo.findAvailablePlants(po.getStartDate(), po.getEndDate(), po.getPlant().getName());
		
		if(!plants.contains(po.getPlant())) {
			throw new PlantNotAvailableException(po.getPlant().getPlantId());
		}
		
		customerRepo.saveAndFlush(customer);
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@ExceptionHandler(PlantNotAvailableException.class)
	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Plant not available!")
	public @ResponseBody void handleNotAvailable(){
		
	}
	
	@ExceptionHandler(NegativeTimeIntervalException.class)
	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Negative time interval!")
	public @ResponseBody void handleBadTimeInterval() {
		
	}
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<PurchaseOrderResource> getAllPurchaseOrders(){
		List<PurchaseOrder> pos = poRepo.findAll();
		return purchaseOrderResourceAssambler.toResource(pos);
	}
	
	@RequestMapping(value="{id}")
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource getPurchaseOrder(@PathVariable("id") Long id) throws Exception{
		PurchaseOrder po = poRepo.findOne(id);
		
		if(po == null) {
			throw new PONotFoundException(id);
		}
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@ExceptionHandler(PONotFoundException.class)
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
	public void handle404(){
		
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource updatePO(@RequestBody PurchaseOrderResource poResource, @PathVariable("id") Long id) throws Exception{

		PurchaseOrder po = poRepo.findOne(poResource.getPoId());
		
		if(poResource.getStartDate().after(poResource.getEndDate()))
		{
			throw new NegativeTimeIntervalException(poResource.getStartDate(), poResource.getEndDate());
		}
		
		po.setStartDate(poResource.getStartDate());
		po.setEndDate(poResource.getEndDate());
		po.setCost(po.getPlant().getPrice() * (DateHelper.getDaysDifference(po.getStartDate(), po.getEndDate()) + 1));
		po.setStatus(PurchaseOrderStatus.PENDING_CONFIRMATION);
		po.setUpdates(new ArrayList<POUpdateRequest>());
		
		List<Plant> plants = plantRepo.findAvailablePlants(po.getStartDate(), po.getEndDate(), po.getPlant().getName());
		
		if(!plants.contains(po.getPlant())) {
			throw new PlantNotAvailableException(po.getPlant().getPlantId());
		}
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@RequestMapping(value="/{id}/accept", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource acceptPO(@PathVariable("id") Long id){
		
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.OPEN);
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@RequestMapping(value="/{id}/accept", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource rejectPO(@PathVariable("id") Long id){
		
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.REJECTED);
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource closePO(@PathVariable("id") Long id){
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.CLOSED);
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@RequestMapping(value="/{id}/updates", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource requestPOUpdate(@PathVariable("id") Long id, @RequestBody Date extensionDate){
		PurchaseOrder po = poRepo.findOne(id);
		
		po.setStatus(PurchaseOrderStatus.PENDING_UPDATE);
		
		POUpdateRequest update = new POUpdateRequest();
		update.setStatus(URStatus.OPEN);
		//update.setEndDate(DateHelper.addDays(po.getEndDate(), 1));
		update.setEndDate(extensionDate);
		
		updateRepo.saveAndFlush(update);
		
		if(po.getUpdates() == null)
		{
			po.setUpdates(new ArrayList<POUpdateRequest>());
		}
		po.getUpdates().add(update);
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@RequestMapping(value="/{poid}/updates/{upid}/accept", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource rejectPOUpdate(@PathVariable("poid") Long poid, @PathVariable("upid") Long upid){
		PurchaseOrder po = poRepo.findOne(poid);
		
		POUpdateRequest updateRequest = null;
		
		for(POUpdateRequest up : po.getUpdates())
		{
			if(up.getId().longValue() == upid.longValue())
			{
				updateRequest = up;
				break;
			}
		}
		
		if(updateRequest != null)
		{
			updateRequest.setStatus(URStatus.REJECTED);
			updateRepo.saveAndFlush(updateRequest);
		}
		po.setStatus(PurchaseOrderStatus.OPEN);
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
	@RequestMapping(value="/{poid}/updates/{upid}/accept", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public PurchaseOrderResource acceptPOUpdate(@PathVariable("poid") Long poid, @PathVariable("upid") Long upid){
		PurchaseOrder po = poRepo.findOne(poid);
		
		POUpdateRequest updateRequest = null;
		
		for(POUpdateRequest up : po.getUpdates())
		{
			if(up.getId().longValue() == upid.longValue())
			{
				updateRequest = up;
				break;
			}
		}
		
		if(updateRequest != null)
		{
			updateRequest.setStatus(URStatus.ACCEPTED);
			updateRepo.saveAndFlush(updateRequest);
		}
		po.setStatus(PurchaseOrderStatus.OPEN);
		
		poRepo.saveAndFlush(po);
		
		return purchaseOrderResourceAssambler.toResource(po);
	}
	
}
