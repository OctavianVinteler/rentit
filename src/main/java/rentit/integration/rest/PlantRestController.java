package rentit.integration.rest;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rentit.exceptions.PlantNotFoundException;
import rentit.integration.dto.PlantResource;
import rentit.integration.dto.PlantResourceAssembler;
import rentit.models.Plant;
import rentit.repositories.PlantRepository;
import rentit.utils.DateHelper;

@RestController
@RequestMapping("/rest/plants")
public class PlantRestController {
	
	@Autowired
	PlantRepository plantRepo;
	
	PlantResourceAssembler plantResourceAssembler = new PlantResourceAssembler();
	
	@RequestMapping
	public List<PlantResource> getAllPlants(){
		List<Plant> plants = plantRepo.findAll();
		
		return plantResourceAssembler.toResource(plants);
	}
	
	@RequestMapping(value="/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantResource getPlant(@PathVariable("id") Long id) throws Exception{
		Plant plant = plantRepo.findOne(id);
		if(plant == null)
		{
			throw new PlantNotFoundException(id);
		}
		
		return plantResourceAssembler.toResource(plant);
	}
	
	@ExceptionHandler(PlantNotFoundException.class)
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
	public void handle404(){
		
	}
	
	//@RequestMapping(value="plantNameLike")
	@RequestMapping(method=RequestMethod.GET, params={"name", "startDate", "endDate"})
	public List<PlantResource> getPlantByNameDate(@RequestParam String name, @RequestParam String startDate, @RequestParam String endDate){
		
		Date startDateCast = DateHelper.getDateFromString("yyyy-MM-dd", startDate);
		Date endDateCast = DateHelper.getDateFromString("yyyy-MM-dd", endDate);
		
		List<Plant> plants = plantRepo.findAvailablePlants(startDateCast, endDateCast, name);
		
		List<PlantResource> plantResources = plantResourceAssembler.toResource(plants);
		
		return plantResources;
	}

}
