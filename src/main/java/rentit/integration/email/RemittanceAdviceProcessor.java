package rentit.integration.email;

import java.io.StringReader;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rentit.models.Invoice;
import rentit.models.PurchaseOrder;
import rentit.models.RemittanceAdvice;
import rentit.repositories.InvoiceRepository;
import rentit.repositories.PurchaseOrderRepository;
import rentit.repositories.RemittanceAdviceRepository;

@Service
public class RemittanceAdviceProcessor {
	
	@Autowired
	InvoiceRepository invRepo;
	
	@Autowired
	RemittanceAdviceRepository raRepo;
	
	@Autowired
	PurchaseOrderRepository poRepo;
	
	public void processRemittanceAdvice(String remittanceAdvice) throws Exception {

		JAXBContext jaxbContext = JAXBContext.newInstance(RemittanceAdvice.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(remittanceAdvice);
        RemittanceAdvice ra = (RemittanceAdvice) unmarshaller.unmarshal(reader);
        
        Invoice invoice = invRepo.finderMethod(ra.getPurchaseOrderRef());
        
        invoice.setPayed(true);
        
        PurchaseOrder po = poRepo.finderMethod(ra.getPurchaseOrderRef());
        po.setRemittanceAdvice(ra);
        
        invRepo.saveAndFlush(invoice);
        raRepo.saveAndFlush(ra);
        poRepo.saveAndFlush(po);
	}

}
