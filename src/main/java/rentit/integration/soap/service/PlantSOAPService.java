package rentit.integration.soap.service;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;

import rentit.integration.soap.dto.PlantResourceAssembler;
import rentit.integration.soap.dto.PlantResourceList;
import rentit.integration.soap.dto.PurchaseOrderResource;
import rentit.integration.soap.dto.PurchaseOrderResourceAssembler;
import rentit.models.Plant;
import rentit.models.PurchaseOrder;
import rentit.models.PurchaseOrderStatus;
import rentit.repositories.PlantRepository;
import rentit.repositories.PurchaseOrderRepository;
import rentit.utils.DateHelper;

@WebService
public class PlantSOAPService {
	
	@Autowired
	PlantRepository plantRepo;
	
	@Autowired
	PurchaseOrderRepository poRepo;
	
	PlantResourceAssembler plantResourceAssembler = new PlantResourceAssembler();
	PurchaseOrderResourceAssembler poResourceAssembler = new PurchaseOrderResourceAssembler();

	@WebMethod
	public PlantResourceList getAllPlants()
	{
		List<Plant> plants = plantRepo.findAll();
		
		PlantResourceList resourceList = new PlantResourceList(plantResourceAssembler.toResource(plants));
		
		return resourceList;
	}
	
	@WebMethod
	public PlantResourceList getPlantsByName(String name, String minimum, String maximum)
	{
		Date minimumDate = DateHelper.getDateFromString("dd-MM-yyyy", minimum);
		Date maximumDate = DateHelper.getDateFromString("dd-MM-yyyy", maximum);
		
		List<Plant> plants = plantRepo.findAvailablePlants(minimumDate, maximumDate, name);
		
		PlantResourceList resourceList = new PlantResourceList(plantResourceAssembler.toResource(plants));
		
		return resourceList;
	}
	
	@WebMethod
	public PurchaseOrderResource createPurchaseOrder(long plantId, String startString, String endString)
	{
		Date startDate = DateHelper.getDateFromString("dd-MM-yyyy", startString);
		Date endDate = DateHelper.getDateFromString("dd-MM-yyyy", endString);
		
		Plant plant = plantRepo.findOne(plantId);
		
		PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		po.setPlant(plant);
		po.setCost((DateHelper.getDaysDifference(startDate, endDate) + 1) * plant.getPrice());
		po.setStatus(PurchaseOrderStatus.PENDING_CONFIRMATION);
		
		poRepo.saveAndFlush(po);
		
		return poResourceAssembler.toResource(po);
		
	}
	
	@WebMethod
	public PurchaseOrderResource updatePurchaseOrder(long poId, String startString, String endString)
	{
		Date startDate = DateHelper.getDateFromString("dd-MM-yyyy", startString);
		Date endDate = DateHelper.getDateFromString("dd-MM-yyyy", endString);
		
		PurchaseOrder po = poRepo.findOne(poId);
		
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		po.setCost((DateHelper.getDaysDifference(startDate, endDate) + 1) * po.getPlant().getPrice());
		po.setStatus(PurchaseOrderStatus.PENDING_CONFIRMATION);
		
		poRepo.saveAndFlush(po);
		
		return poResourceAssembler.toResource(po);
	}
	
	@WebMethod
	public PurchaseOrderResource cancelPurchaseOrder(long poId)
	{
		PurchaseOrder po = poRepo.findOne(poId);
		po.setStatus(PurchaseOrderStatus.CLOSED);
		
		poRepo.saveAndFlush(po);
		
		return poResourceAssembler.toResource(po);
	}
}
