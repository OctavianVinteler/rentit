package rentit.integration.soap.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import rentit.models.PurchaseOrderStatus;
import lombok.Data;

@Data
@XmlRootElement(name="purchase_order")
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseOrderResource {

	Long poId;
	Date startDate;
	Date endDate;
	PlantResource plant;
	Float cost;
	PurchaseOrderStatus status;
}
