package rentit.integration.soap.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="plant")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantResource {

	Long plantId;
    String name;
    String description;
    Float price;
}
