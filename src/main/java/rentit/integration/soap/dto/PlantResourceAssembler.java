package rentit.integration.soap.dto;

import java.util.ArrayList;
import java.util.List;

import rentit.models.Plant;

public class PlantResourceAssembler {

	public PlantResource toResource(Plant plant)
	{
		PlantResource res = new PlantResource();
		res.setPlantId(plant.getPlantId());
		res.setName(plant.getName());
		res.setDescription(plant.getDescription());
		res.setPrice(plant.getPrice());
		return res;
	}
	
	public List<PlantResource> toResource(List<Plant> plants)
	{
		List<PlantResource> ress = new ArrayList<PlantResource>();
		
		for(Plant plant : plants)
		{
			ress.add(toResource(plant));
		}
		
		return ress;
	}
}
