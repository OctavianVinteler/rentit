package rentit.integration.soap.dto;

import java.util.ArrayList;
import java.util.List;

import rentit.models.PurchaseOrder;

public class PurchaseOrderResourceAssembler {

	PlantResourceAssembler plantAssembler = new PlantResourceAssembler();
	
	public PurchaseOrderResource toResource(PurchaseOrder po){
		PurchaseOrderResource res = new PurchaseOrderResource();
		res.setPoId(po.getId());
		res.setStartDate(po.getStartDate());
		res.setEndDate(po.getEndDate());
		res.setPlant(plantAssembler.toResource(po.getPlant()));
		res.setCost(po.getCost());
		res.setStatus(po.getStatus());
		return res;
	}
	
	public List<PurchaseOrderResource> toResource(List<PurchaseOrder> pos){
		List<PurchaseOrderResource> ress = new ArrayList<PurchaseOrderResource>();
		
		for(PurchaseOrder po: pos)
		{
			ress.add(toResource(po));
		}
		
		return ress;
	}
}
