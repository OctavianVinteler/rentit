package rentit.integration.soap.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="plants")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantResourceList {
	
	@XmlElement(name="plant")
	private List<PlantResource> resources;
	
	public PlantResourceList()
	{
		
	}
	
	public PlantResourceList(List<PlantResource> plants)
	{
		this.resources = plants;
	}

}
