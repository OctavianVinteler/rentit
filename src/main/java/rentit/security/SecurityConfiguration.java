package rentit.security;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration {

    static class UnauthorizedEntryPoint implements AuthenticationEntryPoint {
        @Override
        public void commence(HttpServletRequest requesst, HttpServletResponse response,
                org.springframework.security.core.AuthenticationException authenticationException)
                throws IOException, ServletException {
            response.sendError(
                    HttpServletResponse.SC_UNAUTHORIZED,
                    "Unauthorized: Authentication token was either missing or invalid.");
        }
    }

    @Configuration
    protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
        @Autowired
        private DataSource dataSource;

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            auth
            .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(
                        "select username,password, enabled from users where username=?")
                .authoritiesByUsernameQuery(
                        "select username,authority from authorities where username=?");
        }

    }

   @Configuration                                                   
    public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                .antMatchers("/pos/**").hasAnyRole("ADMIN", "USER")
                .antMatchers("/plants/**").hasAnyRole("ADMIN", "USER")
                .antMatchers("/#/**").permitAll()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .defaultSuccessUrl("/welcome")
                .and().logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
        }
    }
   
   @Configuration
   @Order(1)
   public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
       protected void configure(HttpSecurity http) throws Exception {
           UnauthorizedEntryPoint uep = new UnauthorizedEntryPoint();
           http.csrf().disable()
               .antMatcher("/rest/**").authorizeRequests()
               .antMatchers("/rest/plants/**").hasAnyRole("ADMIN", "USER", "REMOTE")
               //.antMatchers("/rest/plants/").hasAnyRole("ADMIN", "USER", "REMOTE")
               //.antMatchers("/rest/plants/**").authenticated()
               .antMatchers("/rest/pos/**").hasAnyRole("ADMIN", "USER", "REMOTE")
               .and()
               .httpBasic().authenticationEntryPoint(uep);
       }
   }
   
   public class BasicSecureSimpleClientHttpRequestFactory extends SimpleClientHttpRequestFactory {
	    @Autowired
	    Credentials credentials;

	    public BasicSecureSimpleClientHttpRequestFactory() {
	    }

	    @Override
	    public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
	        ClientHttpRequest result = super.createRequest(uri, httpMethod);
	        System.out.println(uri);
	        System.out.println(uri.getAuthority());
	        System.out.println(credentials.getCredentials());

	        for (Map<String, String> map: credentials.getCredentials().values()) {
	            String authority = map.get("authority");
	            if (authority != null && authority.equals(uri.getAuthority())) {
	                result.getHeaders().add("Authorization", map.get("authorization"));
	                break;
	            }
	        }

	        if (credentials.getCredentials().containsKey(uri.getAuthority())) {
	        }
	        return result;
	    }
	}

	@Bean
	public ClientHttpRequestFactory requestFactory() {
	    return new BasicSecureSimpleClientHttpRequestFactory();
	}

	@Bean
	@ConfigurationProperties(locations="classpath:META-INF/integration/credentials.yml")
	public Credentials getCredentials() {
	    return new Credentials();
	}

	public static class Credentials {
	    private Map<String, Map<String, String>> credentials = new HashMap<>();
	    public Map<String, Map<String, String>> getCredentials() {
	        return this.credentials;
	    }
	}
}