package rentit.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement(name="invoice")
public class Invoice {
	@Id
    @GeneratedValue
    Long id;
	
	Boolean payed;
	
	@Temporal(TemporalType.DATE)
	Date startDate;
	
	@Temporal(TemporalType.DATE)
	Date endDate;
	
	@Temporal(TemporalType.DATE)
	Date dueDate;
	
	Float totalCost;
	
	String purchaseOrderRef;
	
	Boolean latePayment;
}
