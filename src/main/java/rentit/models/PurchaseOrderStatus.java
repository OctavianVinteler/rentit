package rentit.models;

public enum PurchaseOrderStatus {
	PENDING_CONFIRMATION,
	OPEN,
	REJECTED,
	CLOSED,
	PENDING_UPDATE
}
