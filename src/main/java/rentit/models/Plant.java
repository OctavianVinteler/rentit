package rentit.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Plant {
    @Id
    @GeneratedValue
    Long plantId;

    String name;
    String description;
    Float price;
}
