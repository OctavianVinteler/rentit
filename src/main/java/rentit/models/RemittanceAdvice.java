package rentit.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement(name="remittanceAdvice")
public class RemittanceAdvice {
	
	@Id
    @GeneratedValue
    Long id;
	
	@Temporal(TemporalType.DATE)
	Date datePayed;
	
	Float amountPayed;
	
	String purchaseOrderRef;
}
