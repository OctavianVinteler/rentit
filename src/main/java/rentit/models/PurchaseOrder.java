package rentit.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    @OneToOne
    Customer customer;

    @OneToOne
    Plant plant;

    @Temporal(TemporalType.DATE)
    Date startDate;

    @Temporal(TemporalType.DATE)
    Date endDate;
    
    Float cost;
    
    @Enumerated(EnumType.STRING)
    PurchaseOrderStatus status;
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="po_id")
    List<POUpdateRequest> updates;
    
    @OneToOne
    Invoice invoice;
    
    @OneToOne
    RemittanceAdvice remittanceAdvice;
}